const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../server')
// Configure chai
chai.use(chaiHttp);
chai.should();
describe("Create a new route", () => {
    describe("POST /", () => {
        it("should create a new item", (done) => {
             chai.request(app)
                 .post('/')
                 .send({
                    from: "BRC",
                    to: "CDG",
                    price: 10
                })
                 .end((err, res) => {
                    if (err)
                        done(err)

                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.items.should.be.a('number')
                     
                    console.log(res.body)

                    if (res.body.items === 8)
                        done()
                    else 
                        throw new Error('Create fail')
                  })
         })
    })
})

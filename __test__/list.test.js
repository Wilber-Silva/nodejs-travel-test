const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../server')
// Configure chai
chai.use(chaiHttp);
chai.should();
describe("List routes", () => {
    describe("GET /", () => {
        it("should return list of routes", (done) => {
             chai.request(app)
                 .get('/')
                 .end((err, res) => {
                    if (err)
                        done(err)

                    res.should.have.status(200)
                    res.body.should.be.a('array')
                    res.body[0].should.be.a('object')
                     
                    console.log(res.body)

                    done()
                  })
         })
    })
})

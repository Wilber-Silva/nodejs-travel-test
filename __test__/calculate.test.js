const chai = require('chai')
const chaiHttp = require('chai-http')
const app = require('../server')
// Configure chai
chai.use(chaiHttp);
chai.should();
describe("Calculate Travel", () => {
    describe("GET /", () => {
        it("should calculate best route", (done) => {
             chai.request(app)
                 .get('/GRU/CDG')
                 .end((err, res) => {
                    if (err)
                        done(err)

                    res.should.have.status(200)
                    res.body.should.be.a('object')
                    res.body.bestsRoute.should.be.a('object')
                    res.body.bestsRoute.price.should.be.a('number')
                     
                    console.log(res.body)

                    if(res.body.bestsRoute.price === 40)
                        done()
                    else
                        throw new Error('Calculate fail')
                  })
         })
    })
})

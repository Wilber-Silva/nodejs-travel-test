# README #

Partindo da ideia de Api Rest foram criados 3 endpoints, um para a criação de novas rotas de viagem, um para listagem das novas rotas (para validar a criação) e um para calcular a melhor rota. No calculo da melhor rota como aqui é api estou retornando valores que poderiam ser uteis para um front-end como por exemplo a rota direta sem nenhuma parada caso exista ou as outras opições de rotas que o usuário pode escolher.

* Tentei manter o código bem comentado ( talvez até tenha comentado de mais )
* Optei por não usar docker creio que usando apenas o expres.js e cache pra gravar as alterações já seria válido

* Optei por não usar um arquivo .csv e conserti o mesmo para um array de json para facilitar a leitura.

* Sobre a interface de console a mesma está sendo entregue como teste unitário e a saida ao invés de um .csv é o log exposto no terminal, não vi o por que de usar .csv a não ser que um dos fundatos do teste seja testar se consigo ler ou excrever um .csv via código

## Paths & Files ##

* / - arquivos de configuração
* server.js - arquivo que inicia o servidor

* /src - applicação
  * data-mock.js - corresponde aos dados iniciais anexados ao teste (mudei para json para facilitar o manuseio)
  * route.js - rotas da interface rest

  * /Helpers - mini funções de ajuda
    * express-response-adapter.js - aquivo que adapta as classes de resposta serverless para o express

  * /Controllers - Controladores
    * TravelController.js - Controlador de viagens, responsável por criar, listar e calcular rotas

  * /Services - Interface de serviços ou integração
    * CacheService.js - Interface para uso dos serviços de cache do node

  * /Validations - Schemas de validação
    * TravelSchema.js - Schema de validação dos eventos relacionados a viagens

* /__test__ - Testes unitários
  * calculate.test.js - testa a função que calcula a melhor rota e compara o valor correto
  * create-route.test.js - testa a criação de uma nova rota
  * list.test.js - testa a listagem de todas as rotas

## Dependencies ##

* express: uso apenas para subir o servidor para interface api-rest
* body-parser: usado junto ao 'express' para transformar o corpo da requisição http em json
* node-cache: usado para escrever e ler no cache
* @hapi/joi: usado para não precisar ficar fazendo if ou parse com intuito de validar o evento http
* will-core-lib: lib de minha autoria usado aqui para criar retornos http e tratamento de alguns erros
* mocha - driver de testes unitários
* chai e chai-http - modulos para simular uma requição http

## Requisition ##

O único requisito é ter instalado o node, o desenvolvimento foi feito usando o node v12, mas creio que o projeto seja executado com sucesso em algumas versões inferiores

## Run ##

No terminal dentro da pasta do projeto

* Instale as dependencias do projeto
    npm install

* Subir o servidor node
    npm start

* Executar testes unitários
    npm test

### Api Rest ###

* GET /

-lista todas as rotas

Response:

```json
[
    {
        "from": "GRU",
        "to": "BRC",
        "price": 10
    },
    {
        "from": "BRC",
        "to": "SCL",
        "price": 5
    },
    {
        "from": "GRU",
        "to": "CDG",
        "price": 75
    },
    {
        "from": "GRU",
        "to": "SCL",
        "price": 20
    },
    {
        "from": "GRU",
        "to": "ORL",
        "price": 56
    },
    {
        "from": "ORL",
        "to": "CDG",
        "price": 5
    },
    {
        "from": "SCL",
        "to": "ORL",
        "price": 20
    }
]
```

* POST /

-cria uma nova rota

Request Example:

```json
{
    "from": "GRU",
    "to": "ORL",
    "price": 75
}  
```

Response:
-Como está sendo usado apenas o cache estou retornando a quantidade de itens ao invés de um 'id'

```json
{
    "items": 8
}
```

* GET /:from/:to (example: /GRU/CDG)

dentro de bestsRoute ou possibleRoutes

* "price": Somatória de todos os pontos da viagem
* "route": Pontos da rota final a cada dois pontos temos uma saida e uma parada com o exemplo abaixo

```json
   ["GRU", "BRC", "BRC","SCL"]
```

Significa que uma viagem foi iniciada em GRU teve uma parada em BRC lá mesmo a viagem foi retomada e finalizada em SCL

Response:

```json
{
    "directRoute": {
        "from": "GRU",
        "to": "CDG",
        "price": 75
    },
    "bestsRoute": {
        "price": 40,
        "route": [
            "GRU",
            "BRC",
            "BRC",
            "SCL",
            "SCL",
            "ORL",
            "ORL",
            "CDG"
        ]
    },
    "possibleRoutes": [
        {
            "price": 40,
            "route": [
                "GRU",
                "BRC",
                "BRC",
                "SCL",
                "SCL",
                "ORL",
                "ORL",
                "CDG"
            ]
        },
        {
            "price": 45,
            "route": [
                "GRU",
                "SCL",
                "SCL",
                "ORL",
                "ORL",
                "CDG"
            ]
        },
        {
            "price": 61,
            "route": [
                "GRU",
                "ORL",
                "ORL",
                "CDG"
            ]
        }
    ]
}
```

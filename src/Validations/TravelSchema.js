const Joi = require('@hapi/joi')

const route = Joi.string().max(3).required()
const from = route.disallow(Joi.ref('to'))

module.exports.create = Joi.object({
  from,
  to: route,
  price: Joi.number().required()
})

module.exports.calculate = Joi.object({
  from,
  to: route
})

const { 
    ResponseOK,
    ResponseBadRequestException,
    ResponseUnprocessableEntityException,
    ResponseNotFoundException,
    ResponseError 
} = require('will-core-lib/http') // Sei que a linha ficou grande mas gosto de deixar de forma mais explicita (apenas por gosto)
const { JoiValidation } = require('will-core-lib/validators')
const CacheService = require('./../Services/CacheService')
const TravelRequestSchema = require('./../Validations/TravelSchema')
const { json } = require('../Helpers/express-response-adapter')

class TravelController {
    constructor() {
        this.cacheName = 'routes'
    }
    // apenas pega o que esta em cache e retorna
    list = async (req, res) => {
        try {
            const items = await CacheService.get(this.cacheName)
            return json(res, new ResponseOK(items))
        } catch (error) {
            return json(res, new ResponseError(error))
        }
    }
    /* 
     *** Funcioanmento
        Como estou usando cache optei por pegar o valor atual modificar via código e sobre escrever-lo
        caso esteja tudo certo.

     *** Regras para gravar
        A requisição deve obedecer o shema criado com joi onde todos os valores são obrigatórios em seus
        respequitivos formatos (from, to, price)

        Caso uma rota (from e to) já esteja gravado em cache ela não podera ser gravada novamente
     */
    create = async ({ body }, res) => {
        try {
            const validation = await JoiValidation(TravelRequestSchema.create, body)
            if (!validation.isValid) throw new ResponseBadRequestException(validation.message)

            const items =  await CacheService.get(this.cacheName)

            const duplicate = items.find(item => body.from === item.from && body.to === item.to)
            if (duplicate) throw new ResponseBadRequestException('Already Exists')

            items.push(body)
            await CacheService.set(this.cacheName, items)
            // retornaria aqui o id mas como estou usando cache não tenho o mesmo
            return json(res, new ResponseOK({ items: items.length })) 
        } catch (error) {
            return json(res, new ResponseError(error))
        }
    }
    // Verifica todas as conexões diretas
    haveConnection (to, routes) {
        return routes.filter(route => route.from === to)
    }
    // encontra os pontos possíveis
    findPoints (route, routes, to) {
        // garantia para não passar pera rota direta novamente
        if (route.to === to) return false

        return this.mapPossibles(route, routes)
    }
    // Mapeia e agrupa as possíveis rotas (de forma recursiva)
    mapPossibles (route, routes, newRoute = { price: 0, route: [] }) {
        // acumulador da função recursiva
        newRoute.price = newRoute.price + route.price
        newRoute.route.push(route.from)
        newRoute.route.push(route.to)

        // verifica se o ponto atual tem uma conexão possivel
        const aux = this.haveConnection(route.to, routes)
        if (aux.length){
            aux.map(point => this.mapPossibles(point, routes, newRoute))
            
        }
        return newRoute
    }
    // Cria as possíveis rotas
    plotRoutes (startPoints, routes, to) {
        return startPoints.map(startPoint => this.findPoints(startPoint, routes, to))
    }
    // encontra todas as possibilidades de rotas alternativas
    async possibleRoutes ({ from, to }, routes) {
        const startingPoints = routes.filter(route => route.from === from)
        const possibleRoutes = this.plotRoutes(startingPoints, routes, to).filter(route => route)
        return possibleRoutes
    }
    // procura melhor rota
    async bestsRoutes (directRoute, possibleRoutes, to) {
        directRoute = {
            price: directRoute.price,
            route: [
                directRoute.from,
                directRoute.to
            ]
        }    
        if(possibleRoutes.length){
            let min = possibleRoutes[0]
             // verifica melhor rota dentro das diversas posibilidades
            for (const index in possibleRoutes) {
                
                if (possibleRoutes[index].route.includes(to)) {
                    if (!min.price || min.price > possibleRoutes[index].price)
                        min = possibleRoutes[index]
                }
                else {
                    possibleRoutes.splice(index)
                }
            }
            // compara melhor possiblidade de rota com a rota direta
            if (directRoute && directRoute.price < min.price)
                min = directRoute

            return min
        }
        
        return directRoute
        
    }
    // Função principal do teste retorna caso exista rota direta, todas as outras possibilidades e o melhor caminho
    calculate = async ({ params }, res) => {
        try {
            // garantir sintaxe
            params.from = params.from.toUpperCase()
            params.to = params.to.toUpperCase()

            // validação dos campos
            const validation = await JoiValidation(TravelRequestSchema.calculate, params)
            if (!validation.isValid) throw new ResponseBadRequestException(validation.message)

            // ler cache
            const routes = await CacheService.get(this.cacheName)

            // encontrar rota direta caso exista
            const directRoute = routes.find(item => item.from === params.from && item.to === params.to)

            // encontra rotas possíveis
            const possibleRoutes = await this.possibleRoutes(params, routes)

            // encontra a melhor rota
            const bestsRoute = await this.bestsRoutes(directRoute, possibleRoutes, params.to)

            // retorno final.. Este retorno pode ter a rota sugerida que é uma rota mais barata e a rota direta ao mesmo tempo
            return json(res, new ResponseOK({ directRoute, bestsRoute, possibleRoutes }))
            
        } catch (error) {
            return json(res, new ResponseError(error))
        }
    }
}

// retorna a class já instanciada pois é uma class final e desta forma não preciso instaciar ela na outra ponta
module.exports = new TravelController()
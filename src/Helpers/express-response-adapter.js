/*
    O intuito desse cara é para adaptar as classes de resposta da minha lib no npm 
    que foram criadas olhando apenas o serverless para o express.js
 */
module.exports.send = (res, returned) => res.status(returned.statusCode).send(returned.body)  
module.exports.json = (res, returned) => res.status(returned.statusCode).json(JSON.parse(returned.body))  
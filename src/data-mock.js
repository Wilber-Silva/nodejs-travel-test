module.exports = [
    { from: 'GRU', to: 'BRC', price: 10 },
    { from: 'BRC', to: 'SCL', price: 5 },
    { from: 'GRU', to: 'CDG', price: 75 },
    { from: 'GRU', to: 'SCL', price: 20 },
    { from: 'GRU', to: 'ORL', price: 56 },    
    { from: 'ORL', to: 'CDG', price: 5 },
    { from: 'SCL', to: 'ORL', price: 20 }
]
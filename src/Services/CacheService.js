
const NodeCache = require('node-cache')

/* 
  Interface criada para representar o serviço de cache
*/
class CacheController {
  constructor() {
    const ttlSeconds = 60 * 60 * 1 // 1 hour
    this.key = 'application-nodejs-travel'
    this.cache = new NodeCache({ 
      stdTTL: ttlSeconds,
      checkperiod: ttlSeconds * 0.2,
      useClones: false 
    })
  }
  generateKey = id => `${this.key}-${id}`

  set = async (id, data) => {
    try {
      await this.cache.set(this.generateKey(id), data)
    } catch (error) {
        throw error
    }
  }

  get = async (id) => {
    try {
        const key = `${this.key}-${id}`
        const value = await this.cache.get(key)
        if (value) return value
        
    }catch (error){
        throw error
    }
  }
  flush() {
    this.cache.flushAll()
  }
}


module.exports = new CacheController()
const { Router } = require('express')
const TravelController = require('./Controllers/TravelController')
const CacheService = require('./Services/CacheService')

const data = require('./data-mock')

const router = Router()

// Inicializa o cache com o arquivo data
CacheService.set('routes', data)

router.post('/', TravelController.create)
router.get('/', TravelController.list)
router.get('/:from/:to', TravelController.calculate)


module.exports = router